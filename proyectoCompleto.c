#include <stdio.h>
#include <string.h>

#define SIZE 20
typedef enum { false, true } bool;

bool validateName(char *nombre);
bool validateAge(int edad);

int main(){
	char nombre[SIZE];
	char apellido[SIZE];
	int edad;
	char nombreJoven[SIZE];
	char apellidoJoven[SIZE];
	int edadJoven=130; 
	char nombreMayor[SIZE];
	char apellidoMayores[SIZE];
	int edadMayor=0;
	int suma = 0;
	double promedio;
	
	
	for (int i=0; i < 10; i++){
		printf("\nPersona %d\n", i+1);
		
		do{
			printf("Ingrese su nombre: ");
			scanf("%s", nombre);
		}while(!validateName(nombre));
		
		do{
			printf("Ingrese su apellido: ");
			scanf("%s", apellido);
		}while(!validateName(apellido));
		
		do{
			printf("Ingrese su edad: ");
			scanf("%d", &edad);
		}while(!validateAge(edad));
		
		if (edad >= edadMayor){
			edadMayor = edad;
			strncpy ( nombreMayor, nombre, SIZE );
			strncpy ( apellidoMayores, apellido, SIZE );
		}
		
		if (edad <= edadJoven){
			edadJoven = edad;
			strncpy ( nombreJoven, nombre, SIZE );
			strncpy ( apellidoJoven, apellido, SIZE );
			
		}
		
		
		
		suma = suma+edad;       
	}
	
	promedio = (double)suma/10.0;
	
	printf("\nPromedio de edades: %.2f", promedio);
	printf("\nPersona mas mayor es: %s %s, con edad =  %d", nombreMayor, apellidoMayores, edadMayor);
	printf("\nPersona mas joven es: %s %s, con edad = %d", nombreJoven, nombreJoven, edadJoven);   
	
}


bool validateAge(int edad){
	bool validacion = true;
	if (edad <= 0 || edad > 130){
		validacion = false;
	}
	return validacion;
}
bool validateName(char *nombre){
	bool validacion=true;
	if (nombre[0] < 'A' || nombre[0] > 'Z' ){
		
		validacion = false;
	}
	int i = 1;
	do{
		if (nombre[i] < 'a' || nombre[i] > 'z' ){
			validacion = false;
		}
		i++;
	}while(nombre[i] != '\0');
	
	return validacion;
}


